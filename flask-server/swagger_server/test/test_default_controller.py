# coding: utf-8

from __future__ import absolute_import

from flask import json
from six import BytesIO

from swagger_server.models.contact import Contact  # noqa: E501
from swagger_server.models.email_addresses import EmailAddresses  # noqa: E501
from swagger_server.test import BaseTestCase


class TestDefaultController(BaseTestCase):
    """DefaultController integration test stubs"""

    def test_add_contact(self):
        """Test case for add_contact

        Add a new Contact.
        """
        contact = Contact()
        response = self.client.open(
            '/v2/contact',
            method='POST',
            data=json.dumps(contact),
            content_type='application/json')
        self.assert200(response,
                       'Response body is : ' + response.data.decode('utf-8'))

    def test_create_emails_by_username(self):
        """Test case for create_emails_by_username

        Creates emails for user.
        """
        emails_body = EmailAddresses()
        response = self.client.open(
            '/v2/contact/{username}/emailaddresses'.format(username='username_example'),
            method='POST',
            data=json.dumps(emails_body),
            content_type='application/json')
        self.assert200(response,
                       'Response body is : ' + response.data.decode('utf-8'))

    def test_delete_contact_by_username(self):
        """Test case for delete_contact_by_username

        Deletes contact by username.
        """
        response = self.client.open(
            '/v2/contact/{username}'.format(username='username_example'),
            method='DELETE')
        self.assert200(response,
                       'Response body is : ' + response.data.decode('utf-8'))

    def test_get_contact_by_username(self):
        """Test case for get_contact_by_username

        Find contact by username.
        """
        response = self.client.open(
            '/v2/contact/{username}'.format(username='username_example'),
            method='GET')
        self.assert200(response,
                       'Response body is : ' + response.data.decode('utf-8'))

    def test_get_emails_by_username(self):
        """Test case for get_emails_by_username

        Find emails by username.
        """
        response = self.client.open(
            '/v2/contact/{username}/emailaddresses'.format(username='username_example'),
            method='GET')
        self.assert200(response,
                       'Response body is : ' + response.data.decode('utf-8'))

    def test_get_list(self):
        """Test case for get_list

        List of all contacts in db
        """
        response = self.client.open(
            '/v2/contact/list',
            method='GET')
        self.assert200(response,
                       'Response body is : ' + response.data.decode('utf-8'))

    def test_update_contact(self):
        """Test case for update_contact

        Update an existing contact
        """
        contact = Contact()
        response = self.client.open(
            '/v2/contact',
            method='PUT',
            data=json.dumps(contact),
            content_type='application/json')
        self.assert200(response,
                       'Response body is : ' + response.data.decode('utf-8'))


if __name__ == '__main__':
    import unittest
    unittest.main()
