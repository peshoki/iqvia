import connexion
import six
from sqlalchemy.exc import SQLAlchemyError

from swagger_server.models.contact import Contact  # noqa: E501
from swagger_server.models.email_addresses import EmailAddresses  # noqa: E501
from swagger_server import util

from ContactsModel import db 
db.instance = db.Db(uri='sqlite:///test.db')
from ContactsModel import contacts, emails


def add_contact(contact):  # noqa: E501
    """Add a new Contact.

    Creates new contact. # noqa: E501

    :param contact: A new contact to be created.
    :type contact: dict | bytes

    :rtype: None
    """
    if connexion.request.is_json:
        contact = Contact.from_dict(connexion.request.get_json())  # noqa: E501
    #----start code----
    row = contacts.Table(
        username=contact.username,
        firstname =contact.firstname,
        surname=contact.surname,
        email=contact.email)
    db.instance.add(row)
    try:
        db.instance.commit()
    except SQLAlchemyError as e:
        db.instance.rollback()
        return str(e) , 403


    return 'OK', 200
    #----end code----


def create_emails_by_username(username, emails_body):  # noqa: E501
    """Creates emails for user.

    Creates the emails of a single user # noqa: E501

    :param username: username of contact to update emails.
    :type username: str
    :param emails: Array of emails to be associated with contact.
    :type emails_body: dict | bytes

    :rtype: EmailAddresses
    """
    if connexion.request.is_json:
        emails_body = EmailAddresses.from_dict(connexion.request.get_json())  # noqa: E501
    
    #----start code----
    row = db.instance.get(contacts.Table,username)
    if row == None:
        return 'Not found!', 404
    
    email_addresses = []
    for email in emails_body:
        email_addresses.append(emails.Table(email=email['email']))

    row.email_addresses = email_addresses
    try:
        db.instance.commit()
    except SQLAlchemyError as e:
        db.instance.rollback()
        return str(e) , 403
    #----end code----


def delete_contact_by_username(username):  # noqa: E501
    """Deletes contact by username.

    Deletes a single user # noqa: E501

    :param username: username of contact to delete
    :type username: str

    :rtype: object
    """
    #----start code----
    row = db.instance.get(contacts.Table,username)

    if row == None:
        return 'Not found!', 404
    db.instance.delete(row)    
    try:
        db.instance.commit()
    except SQLAlchemyError as e:
        db.instance.rollback()
        return str(e) , 403
    return 'Deleted!'
    #----end code----


def get_contact_by_username(username):  # noqa: E501
    """Find contact by username.

    Returns a single user # noqa: E501

    :param username: username of contact to return
    :type username: str

    :rtype: Contact
    """
    #----start code----
    row = db.instance.get(contacts.Table,username)

    if row == None:
        return 'Not found!', 404
    return {
        "username": row.username,
        "firstname": row.firstname,
        "surname": row.surname,
        "email": row.email
    }
    #----end code----


def get_emails_by_username(username):  # noqa: E501
    """Find emails by username.

    Returns the emails of a single user # noqa: E501

    :param username: username of contact to return
    :type username: str

    :rtype: EmailAddresses
    """
    #----start code----
    row = db.instance.get(contacts.Table,username)
    if row == None:
        return 'Not found!', 404
    
    email_addresses = []
    for email in row.email_addresses:
        email_addresses.append({"email": email.email})

    return email_addresses, 200
    #----end code----


def get_list():  # noqa: E501
    """List of all contacts in db

    Returns a list of all contasts in db. # noqa: E501


    :rtype: Contact
    """
    #----start code----    
    all_rows = db.instance.get_all(contacts.Table)
    to_ret = []
    for row in all_rows:
        to_ret.append({
            "username": row.username,
            "firstname": row.firstname,
            "surname": row.surname,
            "email": row.email})

    return to_ret
    #----end code----

def update_contact(contact):  # noqa: E501
    """Update an existing contact

     # noqa: E501

    :param contact: Contact to 
    :type contact: dict | bytes

    :rtype: None
    """
    if connexion.request.is_json:
        contact = Contact.from_dict(connexion.request.get_json())  # noqa: E501
    #----start code----
    row = db.instance.get(contacts.Table,contact.username)
    if row == None:
        return 'Not found!', 404
    row.firstname=contact.firstname
    row.surname=contact.surname
    row.email=contact.email
    try:
        db.instance.commit()
    except SQLAlchemyError as e:
        db.instance.rollback()
        return str(e) , 403
    return 'Updated!'
    #----end code----
