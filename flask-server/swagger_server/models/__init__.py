# coding: utf-8

# flake8: noqa
from __future__ import absolute_import
# import models into model package
from swagger_server.models.contact import Contact
from swagger_server.models.email import Email
from swagger_server.models.email_addresses import EmailAddresses
