#!/usr/bin/env python3

import connexion

from swagger_server import encoder
from ContactsModel import db 
db.instance = db.Db(uri='sqlite:///test.db')
from ContactsModel import contacts, emails


def main():
    db.instance.create_db()
    app = connexion.App(__name__, specification_dir='./swagger/')
    app.app.json_encoder = encoder.JSONEncoder
    app.add_api('swagger.yaml', arguments={'title': 'IQVIA Python tech test.'})
    app.run(port=8080)


if __name__ == '__main__':
    main()
