import unittest

from ContactsModel import db 
db.instance = db.Db(uri='sqlite:///test.db')
from ContactsModel import contacts, emails

class MyTest(unittest.TestCase):

    def create_db(self):
        db.instance.create_db()
    
    def add(self):
        db.instance.create_db()
        row = contacts.Table(
            username='joedoe',
            firstname ='Joe',
            surname='Doe',
            email='joedoe@grrr.com')
        db.instance.add(row)

    def commit(self):
        db.instance.commit()
    
    def get(self):
        joedoe = db.instance.get(contacts.Table,'joedoe')
        self.assertEqual(joedoe.firstname,'Joe')
        self.assertEqual(joedoe.surname,'Doe')
        self.assertEqual(joedoe.email, 'joedoe@grrr.com')

    def get_all(self):
        joedoe = db.instance.get_all(contacts.Table)
    
    def delete(self):
        joedoe = db.instance.get(contacts.Table,'joedoe')
        db.instance.delete(joedoe)
        db.instance.commit()

    def add_relations(self):
        email_addresses = [
            emails.Table(email='hhh@ggg.com'),
            emails.Table(email='test@rrrr.com')]
        row = contacts.Table(   
            username='doejoe',
            firstname ='Doe',
            surname='Joe',
            email='doejoe@grrr.com',
            email_addresses = email_addresses)
        db.instance.add(row)
        db.instance.commit()

    def update_relations(self):
        email_addresses = [
            emails.Table(email='doe@joe.com'),
            emails.Table(email='doejoe@gmail.com')]
        doejoe = db.instance.get(contacts.Table,'doejoe')
        doejoe.email_addresses = email_addresses
        db.instance.commit()

    def get_relations(self):
        doejoe = db.instance.get(contacts.Table, 'doejoe')
        self.assertEqual(doejoe.email_addresses[0].email, 'doe@joe.com')
        self.assertEqual(doejoe.email_addresses[1].email, 'doejoe@gmail.com')

    def drop_db(self):
        db.instance.drop_db()
    
    def test_run(self):
        self.create_db()
        self.add()
        self.commit()
        self.get()
        self.get_all()
        self.delete()
        self.add_relations()
        self.update_relations()
        self.get_relations()
        self.drop_db()


if __name__ == '__main__':
    unittest.main()