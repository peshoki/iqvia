from sqlalchemy import Column, String, Integer, ForeignKey
from ContactsModel import db, contacts
from sqlalchemy.orm import relationship

class Table(db.instance.get_base()):
   __tablename__ = 'emails'
   id = Column(Integer, primary_key = True)
   email = Column(String)
   username = Column(String, ForeignKey('contacts.username'))


