from setuptools import setup

setup(name='ContactsModel',
      version='0.4',
      classifiers=[
        'Development Status :: 3 - Alpha',
        'License :: OSI Approved :: MIT License',
        'Programming Language :: Python :: 3.6',
      ],
      author='Peter Kirov',
      author_email='p.kirov@gmail.com',
      license='MIT',
      packages=['ContactsModel'],
      test_suite='nose.collector',
      tests_require=['nose', 'nose-cover3'],
      include_package_data=True,
      install_requires=['sqlalchemy'],
zip_safe=False)