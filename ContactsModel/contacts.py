from sqlalchemy import Column, String, Integer, ForeignKey
from ContactsModel import db, emails
from sqlalchemy.orm import relationship

class Table(db.instance.get_base()):
   __tablename__ = 'contacts'
   username = Column(String, primary_key = True)
   firstname = Column(String)
   surname = Column(String)
   email = Column(String)
   email_addresses = relationship(emails.Table)
