from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker
from sqlalchemy.ext.declarative import declarative_base



class Db():

   def __init__(self, uri='sqlite:///contacts.db'):
      self.engine = create_engine(uri,echo=False)
      session = sessionmaker(bind = self.engine)
      self.session = session()
      self.base = declarative_base()
   
   def get_base(self):
      return self.base

   def create_db(self):
      self.base.metadata.create_all(self.engine)
   
   def drop_db(self):
      self.base.metadata.drop_all(self.engine)

   def add(self,column):
      self.session.add(column)

   def delete(self,column):
      self.session.delete(column)
   
   def get(self, obj, key):
      return self.session.query(obj).get(key)
   
   def commit(self):
      self.session.commit()
   
   def rollback(self):
      self.session.rollback()

   def get_all(self,obj):
      return self.session.query(obj).all()

instance = Db()
