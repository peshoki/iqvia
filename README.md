Instructions
------------

The solution was tested with python 3.6 and 3.7. It may work with older versions, but there is no gurantee.

1. Create and activate virtual environment.
```bash
python3 -m venv .venv
source .venv/bin/activate 
```
2. Test the ContactsModel
```bash
pip install sqlalchemy
python ContactsModel/setup.py test
```
3. Install ContactsModel module.
```bash
python ContactsModel/setup.py install 
```
4. Run flack server.  
I am using [connexion](https://github.com/zalando/connexion) server - this is a wrapper build arround flask server. The code is generated using a [swagger codegen](https://github.com/swagger-api/swagger-codegen).
Files are located in flask-server and there is README file there.
```bash
cd flask-server
python -m swagger_server
```   
Point your browser to http://localhost:8080/v2/ui/. Here you can test all the endpoints.

